﻿namespace IconBuilder
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Windows.Forms;

    public class frmMain : Form
    {
        private Button btnBuilder;
        private Button btnOpen;
        private IContainer components;
        private Label label1;
        private OpenFileDialog openFileDialog1;
        private string path = "";
        private TextBox txtPath;

        public frmMain()
        {
            this.InitializeComponent();
        }

        private void btnBuilder_Click(object sender, EventArgs e)
        {
            if (!this.txtPath.Text.Equals("") && !this.path.Equals(""))
            {
                if (Directory.CreateDirectory(this.path) == null)
                {
                    MessageBox.Show("出错啦！创建文件夹:" + this.path + "失败", "错误", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else
                {
                    bool flag = true;
                    string path = this.path + "iosRes/";
                    //if (Directory.CreateDirectory(path) != null)
                    //{
                    //    flag = ((((((((flag && this.GetThumbnail(this.txtPath.Text, path + "iTunesArtwork", ImageFormat.Png, 0x200)) && this.GetThumbnail(this.txtPath.Text, path + "Icon@2x.png", ImageFormat.Png, 0x72)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-72@2x.png", ImageFormat.Png, 0x90)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-Small-50@2x.png", ImageFormat.Png, 100)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-72.png", ImageFormat.Png, 0x48)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-Small@2x.png", ImageFormat.Png, 0x3a)) && this.GetThumbnail(this.txtPath.Text, path + "Icon.png", ImageFormat.Png, 0x39)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-Small-50.png", ImageFormat.Png, 50)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-Small.png", ImageFormat.Png, 0x1d);
                    //}
                    //path = this.path + "iosRes2/";
                    if (Directory.CreateDirectory(path) != null)
                    {
                        //flag = (((((((((((((flag && this.GetThumbnail(this.txtPath.Text, path + "iTunesArtwork", ImageFormat.Png, 0x200)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-114.png", ImageFormat.Png, 0x72)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-144.png", ImageFormat.Png, 0x90)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-100.png", ImageFormat.Png, 100)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-72.png", ImageFormat.Png, 0x48)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-58.png", ImageFormat.Png, 0x3a)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-57.png", ImageFormat.Png, 0x39)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-50.png", ImageFormat.Png, 50)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-29.png", ImageFormat.Png, 0x1d)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-76.png", ImageFormat.Png, 0x4c)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-152.png", ImageFormat.Png, 0x98)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-120.png", ImageFormat.Png, 120)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-40.png", ImageFormat.Png, 40)) && this.GetThumbnail(this.txtPath.Text, path + "Icon-80.png", ImageFormat.Png, 80);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 29);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 40);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 50);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 57);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 58);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 72);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 76);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 80);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 87);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 100);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 114);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 120);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 144);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 152);
                        flag = flag && this.GetIOS(this.txtPath.Text, path, 180);
                    }
                    string str2 = this.path + "AndroidRes/";
                    if (Directory.CreateDirectory(str2) != null)
                    {
                        //string str3 = str2 + "drawable/";
                        //if (Directory.CreateDirectory(str3) != null)
                        //{
                        //    flag = flag && this.GetThumbnail(this.txtPath.Text, str3 + "gamelogo_thumb.png", ImageFormat.Png, 100);
                        //}

                        //str3 = str2 + "drawable-ldpi/";
                        //if (Directory.CreateDirectory(str3) != null)
                        //{
                        //    flag = flag && this.GetThumbnail(this.txtPath.Text, str3 + "ic_launcher.png", ImageFormat.Png, 36);
                        //}

                        string str3 = str2 + "mipmap-mdpi/";
                        if (Directory.CreateDirectory(str3) != null)
                        {
                            //48
                            flag = flag && this.GetThumbnail(this.txtPath.Text, str3 + "ic_launcher.png", ImageFormat.Png, 180);
                        }
                        str3 = str2 + "mipmap-hdpi/";
                        if (Directory.CreateDirectory(str3) != null)
                        {
                            //72
                            flag = flag && this.GetThumbnail(this.txtPath.Text, str3 + "ic_launcher.png", ImageFormat.Png, 180);
                        }
                        str3 = str2 + "mipmap-xhdpi/";
                        if (Directory.CreateDirectory(str3) != null)
                        {
                            //96
                            flag = flag && this.GetThumbnail(this.txtPath.Text, str3 + "ic_launcher.png", ImageFormat.Png, 180);
                        }
                        str3 = str2 + "mipmap-xxhdpi/";
                        if (Directory.CreateDirectory(str3) != null)
                        {
                            //144
                            flag = flag && this.GetThumbnail(this.txtPath.Text, str3 + "ic_launcher.png", ImageFormat.Png, 180);
                        }
                    }

                    if (Directory.CreateDirectory(this.path) != null)
                    {
                        flag = flag && this.GetIOS(this.txtPath.Text, this.path, 28);
                        flag = flag && this.GetIOS(this.txtPath.Text, this.path, 108);
                    }

                    if (flag)
                    {
                        MessageBox.Show("ICON生成完毕！", "成功", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        Process.Start("Explorer.exe", this.path);
                    }
                    else
                    {
                        MessageBox.Show("出错啦！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                }
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.Filter = "PNG Image|*.png";
            this.openFileDialog1.Title = "打开";
            this.openFileDialog1.Multiselect = false;
            this.openFileDialog1.CheckFileExists = false;
            this.openFileDialog1.FileName = string.Empty;
            this.openFileDialog1.ShowDialog();
            this.txtPath.Text = this.openFileDialog1.FileName;
            this.path = this.openFileDialog1.FileName.Substring(0, this.openFileDialog1.FileName.Length - this.openFileDialog1.SafeFileName.Length);
            this.path = this.path + DateTime.Now.ToString("yyyyMMddhhmmss") + @"\";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        public bool GetThumbnail(string imgPath, string savePath, ImageFormat format, int scaling)
        {
            try
            {
                using (Bitmap bitmap = new Bitmap(imgPath))
                {
                    using (Image image = bitmap.GetThumbnailImage(scaling, scaling, () => false, IntPtr.Zero))
                    {
                        image.Save(savePath, format);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool GetIOS(string imgPath, string savePath, int scaling)
        {
            return this.GetThumbnail(imgPath, savePath + "Icon-" + scaling.ToString() + ".png", ImageFormat.Png, scaling);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(frmMain));
            this.btnOpen = new Button();
            this.btnBuilder = new Button();
            this.label1 = new Label();
            this.txtPath = new TextBox();
            this.openFileDialog1 = new OpenFileDialog();
            base.SuspendLayout();
            this.btnOpen.Location = new Point(0x185, 14);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new Size(0x1f, 0x15);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "...";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new EventHandler(this.btnOpen_Click);
            this.btnBuilder.Location = new Point(0x25, 0x33);
            this.btnBuilder.Name = "btnBuilder";
            this.btnBuilder.Size = new Size(0x166, 0x1b);
            this.btnBuilder.TabIndex = 1;
            this.btnBuilder.Text = "生成图标";
            this.btnBuilder.UseVisualStyleBackColor = true;
            this.btnBuilder.Click += new EventHandler(this.btnBuilder_Click);
            this.label1.AutoSize = true;
            this.label1.Location = new Point(12, 0x11);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x35, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "原图片：";
            this.txtPath.Enabled = false;
            this.txtPath.Location = new Point(0x47, 14);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new Size(0x138, 0x15);
            this.txtPath.TabIndex = 3;
            this.openFileDialog1.FileName = "openFileDialog1";
            base.AutoScaleDimensions = new SizeF(6f, 12f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x1b0, 90);
            base.Controls.Add(this.txtPath);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.btnBuilder);
            base.Controls.Add(this.btnOpen);
            base.FormBorderStyle = FormBorderStyle.Fixed3D;
            base.Icon = (Icon)manager.GetObject("$this.Icon");
            base.MaximizeBox = false;
            base.Name = "frmMain";
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "IconBuilder";
            base.ResumeLayout(false);
            base.PerformLayout();
        }
    }
}

